﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Caracter_move : MonoBehaviour
{
    [Range(.01f, 4.0f)]
    public float gravityMultiplayer = 1.0f;
    [Range(.01f, 5.0f)]
    public float rotMultiplayer = 1.0f;

    public float V0 = 1.0f;

    CharacterController controller;
    bool isFalling = false;
    float fallingtime = 0.0f;
    // Use this for initialization
    void Start()
    {
        //le dices al componente original
        controller = this.GetComponent<CharacterController>();
    }

    /* bool IsGrounded()
    {
        Ray ray = new Ray(this.transform.position, Vector3.down);
        RaycastHit hit;
        bool grounded = Physics.Raycast(ray, out hit, 1.2f); //la linea choco con algo?
        //Debug.Log ("Crashed whith" + hit.collider.gameObject);
        return grounded;
    } */

  /*  float ApplayFall(bool gr)
    {
        if (!isFalling && !gr)
        {
            fallingtime = 0.0f;
        }
        if (isFalling && gr)
        {
            //esta callendo pero tambien es gr
            fallingtime = 0.0f;
            V0 = 0.0f;
        }
        if (isFalling && !gr)
        {
            fallingtime += Time.deltaTime;
        }
        isFalling = !gr;
        return V0 + fallingtime * Physics.gravity.y * gravityMultiplayer;
    }*/

    // Update is called once per frame
    void Update()
    {
       /*bool onGround = IsGrounded();
        if (Input.GetButtonDown("Jump"))
        {
            V0 = .5f;
        }*/


        
        Vector3 rot = this.transform.eulerAngles;
        rot.y += Input.GetAxis ("Horizontal");
        this.transform.eulerAngles = rot;
        //Es lo mismo que:

        /*this.transform.Rotate(new Vector3(0, Input.GetAxis("Horizontal") * rotMultiplayer, 0));
        Vector3 move = this.transform.TransformDirection(Vector3.forward);
        float adv = Input.GetAxis("Vertical");
        controller.Move(move * adv + new Vector3(0, ApplayFall(onGround), 0));*/


        //Vector3 pos = this.transform.position + move*adv;
        
        Vector3 pos = this.transform.position;
        float adv = Input.GetAxis ("Vertical");
        //float rotRad = rot.y * Mathf.Deg2Rad ; // Mathf.PI / 180.0f
       // float dz=Mathf.Cos(rotRad);
        //float dx = Mathf.Sin(rotRad);

      //  pos.z += dz*adv;
        //pos.x += dx*adv;

        this.transform.position = pos;
    }

}
