﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimiento : MonoBehaviour
{
    private Rigidbody rb;
    public float speed;
    public float maxspeed;
    public GameObject referencia;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float moverH=Input.GetAxis("Horizontal");
        //float moverV = Input.GetAxis("Vertical");
        if(rb.velocity.magnitude>maxspeed)
        {
            rb.velocity = rb.velocity.normalized * maxspeed;
        }
        rb.AddForce(moverH * referencia.transform.right * speed);
        //rb.AddForce(moverV * referencia.transform.forward * speed);
    }
}
